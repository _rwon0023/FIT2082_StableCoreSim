import time
import networkx as NX
import numpy as np
import matplotlib.pyplot as plt
import statistics
import csv
from pathlib import Path

from os import listdir
from os.path import isfile, join, dirname

draw_networks = False
cache_cycles = True
use_cache_cycles = True


## MAIN CONFIG
do_write_data = True
do_write_cycle_data = True

calc_connectance = True
calc_clustering = True
calc_shortest_path_length = True
calc_cycles = True
cycle_lim = 5

networks_foldername = "gen network files colle" #"gen network files colle"#"gen network files\\1000 TS, 200 Pop"
use_progress = False
progress_step = 1000

##### GLOBALS
dir = dirname(__file__)
tmp_dir = join(dir,"tmp")
tmp = lambda f:join(tmp_dir,f)
network_files_dir = join(dir,networks_foldername)
data_dir = tmp("data")

prog = []


##### MAIN FUNCTION
def main():
    start_time = time.time()

    graphs = []
    clustering = []
    connectance = []
    shortest_path_length = []
    cycles = []

    network_files = [f for f in listdir(network_files_dir) if isfile(join(network_files_dir, f)) and f[-4:]=='.net']
    network_paths = [join(network_files_dir, f) for f in network_files]
    
    if use_progress:
        progress = load_progress(networks_foldername)
        next_progress = progress+progress_step
        next_progress_complete = next_progress >= len(network_paths)
        network_files = network_files[progress:next_progress]
        network_paths = network_paths[progress:next_progress]
        global prog
        prog = [progress, next_progress]
    
    for i in range(len(network_paths)):
        file = network_paths[i]
        print(i)
        matrix = read_net_file(file)
        
        d_graph = NX.from_numpy_matrix(np.array(matrix),create_using=NX.DiGraph)
        ud_graph = NX.from_numpy_matrix(np.array(matrix))

        graphs.append(d_graph)

        calc(calc_clustering,"Clustering",clustering,lambda :NX.average_clustering(d_graph))
        calc(calc_connectance,"Connectivity",connectance,lambda :graph_connectance(d_graph))
        calc(calc_shortest_path_length,"Average Shortest Path Length",shortest_path_length,lambda :avg_shortest_path_length(ud_graph))
        
        cyc_cache_loaded = False
        if use_cache_cycles:
            try:
                cache = load_cyc_cache(network_files[i])
                calc(calc_cycles,"Cycles",cycles,lambda :cache)
                cyc_cache_loaded = True
            except FileNotFoundError:
                pass
        if cyc_cache_loaded == False:
            calc(calc_cycles,"Cycles",cycles,lambda :cycles_dfs(d_graph,cycle_lim))
            if cache_cycles:
                write_cache_cyc(network_files[i], cycles[i])

    if calc_cycles:
        # process cycle data
        process_cycles(network_files, graphs, cycles)


    data = [["File Name", "Connectivity", "Clustering", "Avg Shortest Path Length"]]
    for i in range(len(graphs)):
        # write metrics data
        d = [network_files[i], connectance[i], clustering[i], shortest_path_length[i]]
        assert len(data[0]) == len(d)
        data.append(d)

        # write cycles data

        # print results
        graph = graphs[i]
        print(bcolors.OKGREEN)
        print('==========================')
        print(network_files[i])
        print(graph)
        print("Average Clustering: ", clustering[i])
        print("Connectance (Edge Density): ", connectance[i])
        print("Average Shortest Path Length: ", shortest_path_length[i])
        print("Cycles: ", len(cycles[i]))

        # draw graph
        if draw_networks:
            NX.draw(graph, with_labels=True)
            plt.show()
        
    print(bcolors.ENDC)

    plt.plot(connectance, shortest_path_length, 'ro')
    plt.savefig(tmp('test.png'))

    if do_write_data: write_csv_data("data",data)

    print(time.time() - start_time)

    if use_progress:
        write_progress(networks_foldername, 0 if next_progress_complete else next_progress)

##### Dataset partitioning

progress_file = tmp("analysis_progress.pr")

def load_progress(name):
    if isfile(progress_file):
        with open(progress_file,'r') as load_file:
            for line in load_file:
                seg = line.split('::',1)
                if seg[0] == name:
                    return int(seg[1])
    return 0

def write_progress(name, prog):
    dataline = f"{name}::{prog}"

    lines = []
    overwritten = False

    if isfile(progress_file):
        with open(progress_file,'r') as f:
            lines = f.readlines()
            for l in range(len(lines)):
                line = lines[l]
                seg = line.split('::',1)
                if seg[0] == name:
                    lines[l] = dataline
                    overwritten = True
                    break
    
    if not overwritten:
        lines.append(dataline)

    with open(progress_file,'w') as result_file:
        result_file.writelines(lines)


##### Cycle caching

def cache_cyc_filename(filename):
    return f"{cycle_lim}_{filename.split('.',1)[0]}.npy"

def write_cache_cyc(filename,cycles):
    folder = tmp(f"cycle_cache\\{networks_foldername}")
    enforce_folderDir(folder)
    filename = cache_cyc_filename(filename)
    np.save(join(folder,filename),cycles)

def load_cyc_cache(filename):
    filename = filename.split('.',1)[0]
    dir = tmp(f"cycle_cache\\{networks_foldername}\\{cache_cyc_filename(filename)}")
    return np.load(dir, allow_pickle=True)


##### Read Files

def read_net_file(file):
    print(file)
    f = open(file, "r")
    matrix = []
    verts = -1
    reading_matrix = False
    reading_arcs = False
    for line in f:
        if len(line)>0:
            if line[0] == '*':
                split_line = line[1:-1].split(' ',1)
                label,args = split_line if len(split_line)==2 else (split_line[0],'')
                if label == "Vertices":
                    verts = int(args.split()[0])
                if label == "Matrix":
                    reading_matrix = True
                    print("Reading Matrix...")
                if label == "Arcs":
                    reading_arcs = True
                    print("Reading Arcs...")
            elif reading_matrix:
                matrix.append([float(n) for n in line.split()])
                verts -= 1
                if verts == 0:
                    reading_matrix = False
            elif reading_arcs:
                if verts != len(matrix):
                    matrix = [[0 for _ in range(verts)] for r in range(verts)]
                data = line.split()
                s,e = int(data[0]),int(data[1])
                v = 0 if len(data) == 2 else int(data[2])
                matrix[s-1][e-1] = v
    f.close()
    return matrix

##### Process Cycles

def process_cycles(network_files, graphs, cycles):
    if do_write_cycle_data:
            # Calc and Write Cycle metrics
            # No Cycles :: Cycle of len 2, 3, 4, etc :: Pos vs Neg :: connectivity scaling with neg/pos ratio

            ##### CYCLES DATA CONFIG
            calc_species_feedback = True
            calc_time_feedback = True 
            calc_centrality = True 
            calc_avgloops = True

            print(bcolors.OKCYAN)
            print('==========================')

            print("Calculating Feedback Loops...")
            ### calculate feedback
            species_feedback_data = [["Species", "Positive Feedback", "Negative Feedback"]]
            time_feedback_data = [["Time", "Positive Feedback", "Negative Feedback"]]
            feedbacks = []
            pos_fb_counts = []
            neg_fb_counts = []
            for i in range(len(graphs)):
                c = cycles[i]
                graph = graphs[i]
                is_pos_fb = []
                for cycle in c:
                    neg_links = 0
                    for v in range(len(cycle)-1):
                        w = graph.get_edge_data(cycle[v], cycle[v+1])['weight']
                        neg_links += (w<0)
                    is_pos_fb.append(neg_links%2==0) # True if pos feedback loop, False if neg feedback loop (neg fb loop has odd neg links)
                num_pos_fb = len(list(filter(lambda b:b, is_pos_fb)))
                num_neg_fb = len(c) - num_pos_fb

                species_feedback_data.append([len(graph), num_pos_fb, num_neg_fb])
                
                ts = network_files[i].split(' TS')[0].split('[')[-1]
                time_feedback_data.append([ts, num_pos_fb, num_neg_fb])

                feedbacks.append(is_pos_fb)
                pos_fb_counts.append(num_pos_fb)
                neg_fb_counts.append(num_neg_fb)

            # Write data
            if calc_species_feedback:
                write_csv_data("cycle_species-feedback_data", species_feedback_data)
            if calc_time_feedback:
                write_csv_data("cycle_time-feedback_data", time_feedback_data)

            cycles_data = [["File Name", "Cycles", "Pos Feedback", "Neg Feedback"]] + [[network_files[i],len(cycles[i]),pos_fb_counts[i],neg_fb_counts[i]] for i in range(len(graphs))]
            write_csv_data("cycles_data", cycles_data)


            if calc_centrality:
                # calc centrality for N/P loops
                print("Calculating Centrality...")
                
                def calc_centr(calc_type_name,calc_func):
                    rank_range = 100
                    btwn_centr_rank_fbs = [[0]*rank_range for _ in range(2)]
                    for i in range(len(graphs)):
                        c = cycles[i]
                        graph = graphs[i]
                        dict_btwn_centr = calc_func(graph)
                        ranked_btwn_centr = sorted(dict_btwn_centr.items(),key=lambda e:-e[1])
                        for j in range(min(rank_range,len(ranked_btwn_centr))):
                            node = ranked_btwn_centr[j][0]
                            for cyc in range(len(c)):
                                cycle = c[cyc]
                                if node in cycle:
                                    is_pos_fb = feedbacks[i][cyc]
                                    btwn_centr_rank_fbs[0 if is_pos_fb else 1][j] += 1
                    centrality_rank_data = [["Rank", "Pos Feedback Loops", "Neg Feedback Loops"]]
                    for i in range(rank_range):
                        centrality_rank_data.append([i+1, 
                            btwn_centr_rank_fbs[0][i],
                            btwn_centr_rank_fbs[1][i]
                        ])
                    # Write data
                    write_csv_data(f"{calc_type_name}_centrality_rank_data",centrality_rank_data)
                
                calc_centr("betweenness",NX.betweenness_centrality)
                calc_centr("degree",NX.degree_centrality)
 
            if calc_avgloops:
                print("Calculating Average Cycles...")
                ### calculate cycle sizes
                cycle_sizes = [[] for _ in range(cycle_lim)]
                posfb_cycle_sizes = [[] for _ in range(cycle_lim)]
                negfb_cycle_sizes = [[] for _ in range(cycle_lim)]
                # cycle_sizes_data = [["File Name"] + [i+1 for i in range(cycle_lim)]]
                for i in range(len(graphs)):
                    c = cycles[i]
                    # Sizes of Cycles
                    cycle_size_count = [0]*cycle_lim
                    posfb_cycle_size_count = [0]*cycle_lim
                    negfb_cycle_size_count = [0]*cycle_lim
                    for j in range(len(c)):
                        cycle = c[j]
                        cycle_size = len(cycle)-1
                        cycle_size_count[cycle_size] += 1
                        is_pos_fb = feedbacks[i][j]
                        if is_pos_fb:
                            posfb_cycle_size_count[cycle_size] += 1
                        else:
                            negfb_cycle_size_count[cycle_size] += 1

                    # cycle_sizes_data.append([network_files[i]] + cycle_size)
                    for i in range(cycle_lim):
                        cycle_sizes[i].append(cycle_size_count[i])
                        posfb_cycle_sizes[i].append(posfb_cycle_size_count[i])
                        negfb_cycle_sizes[i].append(negfb_cycle_size_count[i])
                
                # calc average cycles per size
                avg_cycle_sizes_data = [["Size", "Average Loops", "Average Pos Feedback Loops", "Average Neg Feedback Loops"]]
                for i in range(cycle_lim):
                    avg_cycle_sizes_data.append([i+1, 
                        statistics.mean(cycle_sizes[i]),
                        statistics.mean(posfb_cycle_sizes[i]),
                        statistics.mean(negfb_cycle_sizes[i])
                    ])
                    
                # Write data
                write_csv_data("cycle_size_data",avg_cycle_sizes_data)

            print(bcolors.ENDC)

### Terminal Colour Helper

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

##### Tracked Calculation Handling

def calc(check,name,arr,to_add_func):
    if check:
        print(f"Calculating {name}...")
        try_add(arr,to_add_func)
    else:
        arr.append('N/A')

def try_add(arr,to_add_func):
    try:
        arr.append(to_add_func())
    except Exception as e:
        print (f"{bcolors.FAIL}{e}{bcolors.ENDC}")
        arr.append(None)

##### METRIC CALCULATORS

def avg_shortest_path_length(G):
    nodes = len(G.nodes())
    spls = []
    for i in range(nodes):
        for j in range(nodes):
            if i != j:
                spl = None
                try:
                    spl = NX.shortest_path_length(G,i,j)
                except NX.NetworkXNoPath:
                    spl = 0
                spls.append(spl)
                
    return statistics.mean(spls)

def cycles_dfs(G,max_len):
    def rec_dfs(G,s,node,i):
        node_edges = G.edges(node)
        if i>1 and s == node:
            return [[node]] if i > 2 else [] # don't count 2-edge cycles (self loops)
        elif i == max_len:
            return []
        else:
            paths = []
            for connected in node_edges:
                connected = connected[1]
                paths += [[node]+lower_path for lower_path in rec_dfs(G,s,connected,i+1)]
            return paths
    out = {}
    for node in G.nodes():
        cycles = rec_dfs(G,node,node,1)
        for cycle in cycles:
            out[get_hashable_cycle(cycle)] = cycle
    return list(out.values())

def get_hashable_cycle(cycle): 
    c = cycle[:-1]
    min_i = np.argmin(c)
    if min_i > 0:
        c = c[min_i:] + c[:min_i]
    return tuple(c)

def graph_connectance(G):
    n = len(G.nodes)
    return (len(G.edges))/((n**2) - n)

##### WRITE DATA

def write_csv_data(name, data):
    id = networks_foldername_submost()
    filename = f'[{id}{f" ~ {prog[0]}-{prog[1]}" if use_progress else ""}]_{name}.csv'
    print(f"Writing to {filename}...")
    dirPath = join(data_dir,id)
    enforce_folderDir(dirPath)
    try:
        with open(join(dirPath,filename),'w', newline='') as result_file:
            wr = csv.writer(result_file, dialect='excel')
            wr.writerows(data)
    except PermissionError:
        write_csv_data(name, data)

def enforce_folderDir(folderDir):
    Path(folderDir).mkdir(parents=True, exist_ok=True)

def networks_foldername_submost():
    return networks_foldername.split('\\')[-1]

##### MAIN
if __name__=="__main__":
    main()