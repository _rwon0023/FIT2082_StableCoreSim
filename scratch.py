import numpy as np

def get_hashable_cycle(cycle): 
    c = cycle[:-1]
    min_i = np.argmin(c)
    if min_i > 0:
        c = c[min_i:] + c[:min_i]
    return tuple(c)

print (get_hashable_cycle([1,2,3,4,5,1]))
print (get_hashable_cycle([1,4,3,4,5,1]))
print (get_hashable_cycle([4,3,4,5,1,4]))